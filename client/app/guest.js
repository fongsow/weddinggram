/**
 * Created by Group 3 on 19 Oct
 */
'use strict';

(function () {
    angular
        .module("MyApp")
        .controller("GuestCtrl", MyCtrl);

    MyCtrl.$inject = ["GuestServices", "$stateParams"];

    function MyCtrl(GuestServices, $stateParams) {
        var myCtrl = this;
        
        myCtrl.guessId = $stateParams.guest_id;
        console.log("Login guest at guest controller: ", myCtrl.guessId);

        // var defaultGuestObject = {
        myCtrl.guestObject = {
            name: "",
            email: "",
            contact: "",
            rsvp: 0
        };

        myCtrl.getGuestName = "";
        myCtrl.selectRsvp = "";
        myCtrl.server_response = "";

        //myCtrl.guestObject = angular.copy(defaultGuestObject);
        myCtrl.guests = [];

        function initData() {
            GuestServices.listguests()
                .then(function (result) {
                    console.log("guest list");
                    console.log(result);
                    myCtrl.guests = result;
                })
                .catch(function (err) {
                    console.log("error");
                });
        }

        initData();

        myCtrl.displayRsvp = function displayRsvp(guest) {
            console.log("RSVP" + guest.rsvp);
            if (guest.rsvp == 1) {
                return "Attending";
            } else if (guest.rsvp == 2) {
                return "Not Attending";
            } else {
                return "Pending RSVP";
            }
        }

        myCtrl.registerGuest = function registerGuest() {
            console.log("Sending over");
            console.log(myCtrl.guestObject);
            GuestServices.addGuest(myCtrl.guestObject)
                .then(function (result) {
                    myCtrl.server_response = result;

                })
                .catch(function (result) {
                    myCtrl.server_response = result;
                });

            // myCtrl.guestObject = angular.copy(defaultGuestObject);
        };

        myCtrl.getGuest = function getGuest() {
            console.log("get guest called: " + myCtrl.getGuestName);
            GuestServices.getGuest(myCtrl.getGuestName)
                .then(function (result) {
                    console.log("results : " + JSON.stringify(myCtrl.guests));
                    myCtrl.guests = result;
                })
                .catch(function (error) {
                    console.log(error);
                });
        };
    }
})();

