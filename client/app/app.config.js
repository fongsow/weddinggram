'use strict';
(function () {
    angular
        .module("MyApp")
        .config(MyAppConfig);

    MyAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function MyAppConfig($stateProvider,$urlRouterProvider){

        $stateProvider
            .state('A',{
                url : '/A/:guest_id',
                templateUrl :'../views/photos.html',
                controller : 'PhotoCtrl',
                controllerAs : 'vm'
            })
            .state('B', {
                url: '/B/:guest_id',
                templateUrl: '../views/rsvp.html',
                controller: 'RsvpCtrl',
                controllerAs: 'myCtrl'
            })
            .state('C',{
                url : '/C/:guest_id',
                templateUrl :'../views/guestlist.html',
                controller : 'GuestCtrl',
                controllerAs : 'myCtrl'
            });

        $urlRouterProvider.otherwise("/A/");
    }
})();