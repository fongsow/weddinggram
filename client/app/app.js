/**
 * Created by Group 3 on 19 Oct
 * Updated by Fong Sow
 */

(function() {
    angular.module("MyApp", ["ui.router", "angularModalService", "ngFileUpload" ]);
})();