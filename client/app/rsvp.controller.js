//weddinggram-client-app-rsvp.controller.js file (ie upload.controller.js)

(function () {
    angular
        .module("MyApp")
        .controller("RsvpCtrl", RsvpCtrl);

    RsvpCtrl.$inject = ["RsvpService", "$stateParams"];

    function RsvpCtrl(RsvpService, $stateParams) {
        var vm = this;

        vm.loggedin_guestId = $stateParams.guest_id;
        console.log("Login guest at rsvp controller: ", vm.loggedin_guestId);

        //vm.guest = string;
        vm.guestObject = {
            name: "",
            email: "",
            contact: "",
            pax: 0,
            rsvp: 2
        };

        vm.getGuestInfo = function () {
            console.log("getGuestInfo ", vm.loggedin_guestId);
             RsvpService.getGuestInfo (vm.loggedin_guestId)
             .then(function (result) {
                vm.guestObject = result;
             })
             .catch(function(result) {
                 console.log ("Cannot retrieve guest info: ", result);
             });
        };
        vm.getGuestInfo();


        vm.submit = function () {
            console.log(vm.guestObject);
            RsvpService.updateGuestInfo(vm.guestObject)
             .then(function (resp) {
                 console.log("getGuestInfo updated");
             });
        };

        /*
         vm.refreshname = function () {
         UploadService
         .refreshname()
         .then(function (name) {
         vm.names = name;
         })
         };

         vm.deletename = function () {
         RsvpService
         .deleteGuestname()
         .then(function () {
         vm.Guestname = [];
         vm.status.message = "Guest name been deleted.";
         vm.status.code = 202;
         })
         .catch(function(){
         vm.rsvp.message = "Fail to delete your Guestname.";
         vm.status.code = 400;
         })
         };
         */
    }
})();







