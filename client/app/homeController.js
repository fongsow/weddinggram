/**
 * Created by s17465 on 4/11/2016.
 */
'use strict';
(function () {
    angular
        .module("MyApp")
        .controller("HomeCtrl", HomeCtrl);

    HomeCtrl.$inject = ['ModalService', "GuestServices"];

    function HomeCtrl(ModalService, GuestServices) {
        var vm = this;

        // exposed methods
        vm.showUploadWindow = showUploadWindow;
        vm.nameList = getNameList;
        vm.login = setLoggedInGuest;

        vm.loggedin_guest = {};
        vm.guestList = [];

        console.log("Entered Home Controller");

        function showUploadWindow() {
            console.log("showUploadWindow");
            ModalService.showModal({
                templateUrl: "../views/photoUpload.html",
                controller: "ImageUploadCtrl as uploader",
                inputs: {
                    title: "testing"
                }
            }).then(function (modal) {
                console.log("modal Service OK");
                modal.element.modal();
                modal.close.then(function (result) {

                });
            });
        }

        function setLoggedInGuest() {
            console.log("New Guest logged in", vm.loggedin_guest);
            //vm.loggedin_guest = vm.guestList[vm.loggedin_guestIndex];
            // inform server to update session info
            GuestServices.updateLoginGuest(vm.loggedin_guest)
                .then(function (result) {
                    console.log("Guest login registered to server", result);
                })
                .catch(function (result) {
                    console.log("Server failed to login guest", result);
                });
        }

        function getNameList() {
            console.log("calling Guest Services to get name list");
            GuestServices.getNameList()
                .then(function (result) {
                    vm.guestList = result.namelist;
                    vm.loggedin_guest = result.loggedin_guest;

                    /*// find index of current loggedin guest within the guest list
                     vm.guestList.forEach(function(item, index) {
                     if (vm.loggedin_guest.id == item.id) {
                        vm.loggedin_guestIndex = index;
                     }
                     });*/

                    console.log("HomeCtrl received name list", vm.guestList);
                    console.log("Logged in guest" + vm.loggedin_guest );
                })
                .catch(function (result) {
                    console.log("No name list ", result);
                });
        }
        getNameList();
    }
})();

