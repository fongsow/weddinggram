/*
 Client: photos.js in the root/app

 The client controller is VM of photos.html
 */
'use strict';

(function () {
    angular
        .module("MyApp")
        .controller("PhotoCtrl", PhotoCtrl);

    PhotoCtrl.$inject = ["PhotoServices", "$stateParams"];

    function PhotoCtrl(PhotoServices, $stateParams) {
        var vm = this;

        vm.guessId = $stateParams.guest_id;
        console.log("Login guest at photo controller: ", $stateParams);

        vm.photos = [];
        vm.photo = {};
        vm.cloudURL = "";

        var defaultPhoto = {
            id: 0,
            url: "",
            creator_name: "",
            photo_likes: 0,
            comments: ""
        };

        function mapData(serverData) {
            serverData.forEach(function (item) {

                vm.photos.push({
                    id: item.id,
                    url: vm.cloudURL + item.url,
                    guest_name: item.guest.name,
                    photo_likes: item.photo_likes,
                    comments: item.comments
                })
            })
        }

        function initData() {
            PhotoServices.getPhotoCloudURL()
                .then(function (result) {
                    vm.cloudURL = result.url;
                });

            PhotoServices.ListPhotos()
                .then(function (result) {
                    mapData(result);
                })
                .catch(function (result) {
                    console.log("List Photo Error ", result);
                });
        }


        // To initialize the vm.photos with database Photos
        initData();

        //Adding Interactive
        vm.clicklikes = function (photoIndex) {
            vm.photos[photoIndex].photo_likes++;

            PhotoServices.updateLikes({
                photo_id: vm.photos[photoIndex].id,
                photo_likes: vm.photos[photoIndex].photo_likes
            })
                .then(function (result) {
                    //console.log("Increased Like Count ", result);
                })
                .catch(function (result) {
                    console.log("Like Count Error ", result);
                });

        }
    }
})();
