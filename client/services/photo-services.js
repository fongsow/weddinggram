/**
 * Created by KM on 31-Oct-16.
 */

'use strict';

(function () {
    angular
        .module("MyApp")
        .service("PhotoServices", PhotoServices);

    PhotoServices.$inject = ["$http", "$q"];

    //console.log("Entered Photo Services");
    function PhotoServices($http, $q) {
        var obj = {};

        obj.getPhotoCloudURL = function() {
            var deferred = $q.defer();
            $http
                .get("/api/aws/url")
                .then(function(response){
                    deferred.resolve(response.data);
                })
                .catch(function(response){
                    deferred.reject(response.data);
                })
            return deferred.promise;
        };


        obj.ListPhotos = function() {
            var deferred = $q.defer();
            $http
                .get("/api/photos")
                .then(function (response) {
                    deferred.resolve(response.data);
                }).catch(function (response) {
                deferred.reject(response.data);
            });
            return deferred.promise;
        };

        obj.updateLikes = function(photo_info) {
            var deferred = $q.defer();
            $http
                .put("/api/photos/likes",photo_info)
                .then(function (response) {
                    //console.info("success http updateLikes", response);
                    deferred.resolve(response.data);
                }).catch(function (response) {
                //console.info("error http updateLikes", response);
                deferred.reject(response.data);
            });
            return deferred.promise;
        };

        return obj;
    }
})();
