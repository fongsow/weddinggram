'use strict';

(function () {
    angular
        .module("MyApp")
        .service("GuestServices", GuestServices);

    GuestServices.$inject = ["$http", "$q"];

    console.log("Entered Guest Services");
    function GuestServices($http, $q) {
        var obj = {};

        obj.addGuest = function(guestObject) {
            var deferred = $q.defer();
            $http
                .post("/api/guest",guestObject)
                .then(function (response) {
                    deferred.resolve(response.data);
                })
                .catch(function (response) {
                    deferred.reject(response.data);
                });
            return deferred.promise;
        };

        obj.listguests = function() {
            var deferred = $q.defer();
            $http
                .get("/api/guestlist")
                .then(function (response) {
                    console.log("listguest-services log ");
                    deferred.resolve(response.data);
                }).catch(function (error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        obj.getGuest = function (guestname) {
            var deferred = $q.defer();
            console.log("guest-services log : " + guestname);
            $http
                .get("/api/guestlist/"+guestname)
                // .get("/api/guestlist1", {
                //      params: {'name': guestname}
                //  })

                .then(function (response) {
                    console.log("Success: " + response.data);
                    deferred.resolve(response.data);
                }).catch(function (error) {
                     deferred.reject(error.data);
                 });
            return deferred.promise;
        };

        obj.updateLoginGuest = function(guest) {
            var deferred = $q.defer();
            console.log("calling REST API to login guest");
            $http
                .put("/api/login", guest)
                .then(function (response) {
                    console.log("success http login", response);
                    deferred.resolve(response.data);
                }).catch(function (response) {
                console.log("error http login", response);
                deferred.reject(response);
            });
            return deferred.promise;
        };

        obj.getNameList = function() {
            var deferred = $q.defer();
            console.log("calling REST API to get name list");
            $http
                .get("/api/namelist")
                .then(function (response) {
                    console.log("success http nameList", response);
                    deferred.resolve(response.data);
                }).catch(function (response) {
                console.log("error http namelist", response);
                deferred.reject(response);
            });
            return deferred.promise;
        };
        
        return obj;
    }
})();

