//rsvp.js - ie app.service.js file

'use strict';

(function () {
    angular
        .module("MyApp")
        .service("RsvpService", RsvpService);

    RsvpService.$inject = ["$http", "$q"];

    function RsvpService($http, $q) {

        var vm = this;

        vm.getGuestInfo = getGuestInfo;
        vm.updateGuestInfo = updateGuestInfo;

        function getGuestInfo(guestId) {
            var deferred = $q.defer();
            console.log("calling REST API to get guest record", guestId);
            $http
                .get("/api/guest/" + guestId)
                .then(function (response) {
                    console.log("success http get guest record", response);
                    deferred.resolve(response.data);
                }).catch(function (response) {
                console.log("error http guest record", response);
                deferred.reject(response);
            });
            return deferred.promise;
        }

        function updateGuestInfo(dataObject) {
            var deferred = $q.defer();
            console.log("calling REST API to update guest record",dataObject);
            $http
                .put("/api/guest/", dataObject)
                .then(function (response) {
                    console.log("success http put guest record", response);
                    deferred.resolve(response.data);
                }).catch(function (response) {
                console.log("error http guest update", response);
                deferred.reject(response);
            });
            return deferred.promise;
        }
    }
})();