/**
 * Created by jtc on 28/10/2016.
 */
'use strict';

module.exports = {
    mysql: {
        host:     'localhost',
        username: 'root',
        password: 'mysql',
        database: 'weddinggram'
    }
};