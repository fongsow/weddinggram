/**
 * Created by Group 3 on 19 Oct 2016
 */
const express = require("express");
const app = express();

// parse body of req
const bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

const session = require("express-session");
app.use(session({
    secret: "1111",
    resave: false,
    saveUninitialized: true
}));

app.use(function (req, res, next) {
    if (!req.session.loggedin_guest) {
        req.session.loggedin_guest = { id: 0, name: "" };
    }
    next();
});

const routes = require('./routes');
routes.init(app);
routes.errorHandler(app);

app.set("port", process.argv[2] || process.env.APP_PORT || 3000);
app.listen(app.get("port"), function () {
    console.log("Server started on port %d", app.get("port"));
});

