/*
 File: photo.model.js in the root/model folder

 Define:
 1. The PhotoTable Model
    Primary Key: id
    creator_id link to the guest database
*/

var Sequelize = require("sequelize");

module.exports = function (database) {
    var Photo =
        database.define("photos", {
            url: {type: Sequelize.STRING},
            creator_id: {type: Sequelize.STRING},
            photo_likes: {type: Sequelize.INTEGER},
            comments: {type: Sequelize.TEXT}
        }, {
            timestamps: true
        });

    return Photo;
};
