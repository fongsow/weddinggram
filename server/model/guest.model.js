/**
 * Created by s17465 on 26/10/2016.
 */
var Sequelize = require("sequelize");

module.exports = function (database) {
    var Guest =
        database.define("guests", {
            name: {type: Sequelize.STRING},
            email: {type: Sequelize.STRING},
            contact: {type: Sequelize.STRING},
            rsvp: {type: Sequelize.INTEGER},
            pax: {type: Sequelize.INTEGER}

        }, {
            timestamps: true
        });

    return Guest;
};
