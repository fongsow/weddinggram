/**
 * Created by KM on 31-Oct-16.
 */
'use strict';
var GuestModel = require('../../database').GuestTable;

exports.addGuest = function (req, res) {
    console.log(req.body);
    console.log(req.query);
    console.log(req.params);

    if(req.body.reply == 1){
        res.json(req.body);
    }
    else {
        res.status(500).json(req.body);
    }
};

exports.updateLogin = function (req, res) {
    console.log(req.body);
    console.log(req.query);
    console.log(req.params);

    console.log("new user login");

    req.session.loggedin_guest = req.body;

    res.json(req.session.loggedin_guest);
};

exports.getNameList = function (req, res) {
    console.log("retrieve name list from DB ", req.body, req.query, req.params);

    GuestModel.findAll({
            order: 'name ASC',
            attributes: ['id','name']
        })
        .then(function (guestlist) {
            //console.log(guestlist);
            if (guestlist) {
                var namelist = [];
                guestlist.forEach(function (name) {
                    namelist.push({
                        id: name.dataValues.id,
                        name: name.dataValues.name
                    });
                });
            }
            console.log(namelist);

            res.json({
                loggedin_guest: req.session.loggedin_guest,
                namelist: namelist
            });
        })
        .catch(function (error) {
            console.log("errors retriving namelist", error);
            res.status(500).json(error);
        });
};

