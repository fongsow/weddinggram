/**
 * Created by s17465 on 29/10/2016.
 */
'use strict';

var PhotoModel = require('../../database').PhotoTable;
var GuestModel = require('../../database').GuestTable;

// ListAll Version : Return all the Photo Results w/o Creator Name
exports.listAllv1 = function (req, res) {

    PhotoModel.findAll({})
        .then(function (photos) {
            res.json(photos)
        }).catch(function (error) {
        res.status(500)
            .status(error)
    });
};

// List All:
// Return all the Photo Results with Creator Name and
//    arranged in latest photo first
exports.listAll = function (req, res) {
    console.log("inside List All");
    PhotoModel.findAll({
        order: 'updatedAt DESC',
        include: [{model: GuestModel, attributes: ['name']}]
    })
        .then(function (photos) {
            //console.log(photos);
            res.json(photos)
        }).catch(function (error) {
        console.log("errors retriving");
        //console.log(error);
        res.status(500).json(error);
    });
};

exports.updateLikes = function (req, res) {
    PhotoModel.find({
        where: {
            id: req.body.photo_id
        }
    }).then(function (responseX) {
        //console.log(responseX.dataValues);
        responseX.update(
            {
                photo_likes: req.body.photo_likes
            })
            .then(function (responseY) {
                //console.log(responseY.dataValues);
                res.json(responseY);
            })
            .catch(function (error) {
                res.status(500).json({error: true});
            })
    });
};

exports.createPhoto = function (req, res) {
    console.log("createPhoto");
    console.log(req.body);
    PhotoModel.create({
        "url": req.body.url,
        "creator_id": req.session.loggedin_guest.id,
        "photo_likes": 1,
        "comments": req.body.comment
    }).then(function (result) {
        console.log("createPhoto OK ", result.dataValues);
        res.json(result.dataValues).end();
    }).catch(function (err) {
        res.status(500).json({error: err});
    })
};


