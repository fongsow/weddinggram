/**
 * Created by s25854 on 4/11/2016.
 */
'use strict';

var GuestModel = require('../../database').GuestTable;

exports.addGuest = function (req, res) {
    GuestModel.create(
        {
            name: req.body.name,
            email: req.body.email,
            contact: req.body.contact,
            rsvp: req.body.rsvp
        })
        .then(function (results) {
            console.log(results);
            res.status(200).send("Record Added");
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).send("Fail to add record");

        });
};

exports.listguests = function (req, res) {
    GuestModel.findAll()
        .then(function (response) {
            console.log("listguests called");
            res.json(response);
        }).catch(function (error) {
        res.status(500).send({'error': error});
    });
};
exports.getGuest = function (req, res) {
    var where = {};
    //where.name = req.query.name;
    // where.name = {$like: "%" + req.query.name + "%"};
    where.name = {$like: "%" + req.params.name + "%"};

    console.log("Before getGuest value: " + JSON.stringify(req.query));

    GuestModel.findAll({
        where : where
    })
        .then(function (response) {
            console.log("getGuest value: " + req.query.name);
            res.json(response);
        })
        .catch(function (error) {
            res.status(500).send({'error': error});
        });
}

exports.getGuestInfo = function (req, res) {
    console.log("getGuestInfo", req.query, req.body, req.params);
    GuestModel.findOne({
        where: {id: req.params.guestId},
        attributes: ['id','name', 'email', 'contact', 'rsvp', 'pax']
    })
        .then(function (response) {
            console.log("retrieved record from server");
            res.json(response);
        }).catch(function (error) {
        res.status(500).send({'error':error});
    });
};


exports.updateGuest = function (req, res) {
    var where = {};
    where.id=req.body.id
    GuestModel.update(
        {
            name: req.body.name,
            email: req.body.email,
            contact: req.body.contact,
            rsvp: req.body.rsvp,
            pax: req.body.pax,
        },
    {where: where})

        .then(function (results) {
            console.log(results);
            res.status(200).send("Record updated");
        })
        .catch(function (err) {
            console.log(err);
            res.status(500).send("Fail to update record");

        });
};



