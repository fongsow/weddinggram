/**
 * Created by s17465 on 29/10/2016.
 */
/* Data Seeding */

var Sequelize = require('sequelize');

var database = new Sequelize('weddingDB', 'root', 'myrootpwd', {
    host: "localhost",
    pool: {min: 0, max: 4, idle: 1000}
});

database.authenticate()
    .then(function (result) {
        console.log("OK");
    })
    .catch(function (err) {
        console.log("NOK", err);
    });

/* Seed Data for Photos */
var photoModel = require('./model/photo.model')(database);
var GuestModel = require('./model/guest.model')(database);

database.sync({
    logging: console.log,
    force: true
}).then(function () {

    console.log("Database is in Sync");

    photoModel.create(
        {
            "url": "/guestphotos/a.jpg",
            "creator_id": "1",
            "photo_likes": 1,
            "comments": "This is taken on April at xxx"
        });

    photoModel.create(
        {
            "url": "/guestphotos/b.jpg",
            "creator_id": "2",
            "photo_likes": 10,
            "comments": "This is taken on April at fun"
        });

    photoModel.create(
        {
            "url": "/guestphotos/c.jpg",
            "creator_id": "1",
            "photo_likes": 10,
            "comments": "This is taken on April at fun"
        });

    photoModel.create(
        {
            "url": "/guestphotos/d.jpg",
            "creator_id": "2",
            "photo_likes": 10,
            "comments": "This is taken on April at fun"
        });

    photoModel.create(
        {
            "url": "/guestphotos/e.jpg",
            "creator_id": "4",
            "photo_likes": 10,
            "comments": "This is taken on May at fun"
        });

    GuestModel.bulkCreate([
        {
            name: "Ryan Mennon",
            email: "W@W1.com",
            contact: "98610001"
        },
        {
            name: "Candy Wang",
            email: "W@W2.com",
            contact: "86893003"
        },
        {
            name: "Justin/Joan Tan",
            email: "W@gmail.com",
            contact: "86581007"

        },
        {
            name: "Kathy Dyer",
            email: "kathyD@yahoo.com",
            contact: "86971234",
        },
        {
            name: "Mary Ong",
            email: "MaryOng@yahoo.com.sg",
            contact: "94178909",
        }

    ]).then(function () {
        console.log("Bulk Create OK")
    }).else(function (error) {
        consol.log(error)
    })

}).catch(function (Error) {
    console.log("Error in data creation");
    console.log(Error)
});