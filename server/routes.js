/**
 * Created by Group 3 on 31-Oct-16.
 */
'use strict';

const express = require("express");

const path = require("path");
const docroot = path.join(__dirname, "..");
const CLIENT_FOLDER = path.join(docroot, "client");
const BOWER_FOLDER = path.join(docroot, "bower_components");

const UserController = require("./api/user/user.controller");
const GuestController = require("./api/guest/guest.controller");
const PhotoController = require("./api/photo/photo.controller");
const AWSController = require("./api/aws/aws.controller");

module.exports = {
    init: configureRoutes,
    errorHandler: errorHandler
};

function configureRoutes(app){
    app.post("/api/users", UserController.addGuest);
    app.get("/api/namelist", UserController.getNameList);
    app.put("/api/login", UserController.updateLogin);

    app.get("/api/photos", PhotoController.listAll);
    app.put("/api/photos/likes", PhotoController.updateLikes);

    app.post("/api/guest", GuestController.addGuest);
    app.put("/api/guest", GuestController.updateGuest);
    app.get("/api/guestlist/:name" , GuestController.getGuest);
    // app.get("/api/guestlist1" , GuestController.getGuest);
    app.get("/api/guestlist", GuestController.listguests);
    app.get("/api/guest/:guestId", GuestController.getGuestInfo);

    app.post("/api/photos/upload", PhotoController.createPhoto);
    app.get("/api/aws/url", AWSController.getCloudURL);
    app.post("/api/aws/s3-policy", AWSController.getSignedPolicy);

    app.use(express.static(CLIENT_FOLDER));
    app.use("/bower_components",express.static(BOWER_FOLDER));
//    console.log(CLIENT_FOLDER);
//    console.log(BOWER_FOLDER);
}

function errorHandler(app) {
    app.use(function (req, res) {
        console.log("Client Error 400", req.headers);
        //res.status(401).sendFile(CLIENT_FOLDER + "/app/errors/404.html");
    });

    app.use(function (err, req, res, next) {
        console.log("Server Error 500",err);
        //res.status(500).sendFile(path.join(CLIENT_FOLDER + '/app/errors/500.html'));
    });
}


