/*
  File: database.js in the root folder

  Define:
   1. Database Connection
   2. The Data Models - Database Table
 */

var Sequelize = require('sequelize');
var config = require('./config');

//conn equiv now database
var database = new Sequelize(
    config.mysql.database,
    config.mysql.username,
    config.mysql.password, {
        host: config.mysql.host,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

var PhotoModel = require('./model/photo.model')(database);
var GuestModel = require('./model/guest.model')(database);
//PhotoModel.hasOne(GuestModel, {foreignKey: 'creator_id'});
PhotoModel.belongsTo(GuestModel, {foreignKey: 'creator_id'});

module.exports = {
    GuestTable: GuestModel,
    PhotoTable: PhotoModel

};

